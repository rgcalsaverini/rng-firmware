/*
 * File:   rng.c
 * Author: Rui Calsaverini
 *
 * Created on October 4, 2020, 6:44 PM
 */
#ifndef RNG_H
#define	RNG_H

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#define MIN_PRESS 200      // Less than this will be debounced
#define LONG_PRESS 15000   // More than this will be treated as a long press
#define SLEEP_AFTER 250    // After this many overflows, will turn off display

/* Maximum values of each mode */
uint16_t mode_mod[] = {
    10000,
    100,
    10,
    65535,
};

/* Numerical base of each mode */
uint8_t mode_base[] = {
    10,
    10,
    10,
    16,
};

/* Number of digits of each mode */
uint8_t mode_digits[] = {
    4,
    2,
    1,
    4,
};

/**
 Enable the define below to use a common Anode display, leave it out to use
 a common Cathode display.
 
 #define ANODE
*/
        
#ifdef ANODE
    uint8_t decode_matrix[] = {
        /* 0  */ 0b00000011,
        /* 1  */ 0b10011111,
        /* 2  */ 0b00100101,
        /* 3  */ 0b00001101,
        /* 4  */ 0b10011001,
        /* 5  */ 0b01001001,
        /* 6  */ 0b01000001,
        /* 7  */ 0b00011111,
        /* 8  */ 0b00000001,
        /* 9  */ 0b00011001,
        /* 10 */ 0b00010001,
        /* 11 */ 0b11000001,
        /* 12 */ 0b01100011,
        /* 13 */ 0b10000101,
        /* 14 */ 0b01100001,
        /* 15 */ 0b01110001,
    };
#else
    uint8_t decode_matrix[] = {
        /* 0  */ 0b11111100,
        /* 1  */ 0b01100000,
        /* 2  */ 0b11011010,
        /* 3  */ 0b11110010,
        /* 4  */ 0b01100110,
        /* 5  */ 0b10110110,
        /* 6  */ 0b10111110,
        /* 7  */ 0b11100000,
        /* 8  */ 0b11111110,
        /* 9  */ 0b11100110,
        /* 10 */ 0b11101110,
        /* 11 */ 0b00111110,
        /* 12 */ 0b10011100,
        /* 13 */ 0b01111010,
        /* 14 */ 0b10011110,
        /* 15 */ 0b10001110,
    };
#endif
    
/* Pin map */
#define PIN_SR_DATA  RC4
#define PIN_SR_CLOCK RC3
#define PIN_D1       RA4
#define PIN_D2       RC0
#define PIN_D3       RC5
#define PIN_D4       RA5
#define PIN_BUTTON   RC2

/**
  @Summary
    Generates a random number from 0 to mod.

  @Description
    Generates a random 16-bit unsigned integer ranging from 0 to mod,
    and using a provided salt to increase entropy.

  @Param
   salt - a source of entropy for the random number generator
   mod - the upper limit of the random number

  @Returns
    a random 16-bit unsigned integer
*/
uint16_t RNG_generateRandomNumber(uint16_t salt, uint16_t mod);

/**
  @Summary
    Outputs a number into the 4 Digit display.

  @Description
    Given a number, it's base and the number of digits to display, will encode
    it and output it to the display by keying the shift register and alternating
    the display digit.

  @Param
   number - the number to be displayed
   num_base - the base of the number (currently either 10 or 16, for
              decimal and hexadecimal respectively) 
   digits -  The number of digits to display, from 1 to 4

  @Returns
   None
*/
void RNG_display(uint16_t number, uint8_t num_base, uint8_t digits);

/**
  @Summary
    Outputs a single digit into the display.

  @Description
    Outputs a single digit into the display by keying the shift register.
    Only the segments are outputed, so you are expected to select light the
    specific digit afterwards.
    The number is assumed as being on base 16 (as it is the superset) and will
    be decoded before being outputed.

  @Param
   number - the number to be displayed

  @Returns
   None
*/
void RNG_displayOneDigit(uint8_t number);

/**
  @Summary
    Similar to RNG_displayOneDigit, but does not decode. Mostly for debugging

  @Description
    Outputs a single digit into the display by keying the shift register.
    Only the segments are outputed, so you are expected to select light the
    specific digit afterwards.
    Unlike RNG_displayOneDigit, this is not decoded, meaning, the number will be
    interpreted as a bit-by-bit representation of the segments.

  @Param
   number - the number to be displayed

  @Returns
   None
*/
void RNG_displayOneRaw(uint8_t value);


/**
  @Summary
    Activates a particular set of digits.

  @Description
    Takes a boolean value for all 4 digits, and turns them on or off depending
    on the provided state.

  @Param
   d1 - state of the first (least significant) digit
   d2 - state of the second digit
   d3 - state of the third digit
   d4 - state of the fourth (most significant) digit

  @Returns
   None
*/
void RNG_lightDigit(bool d1, bool d2, bool d3, bool d4);

/**
  @Summary
    Activates one single digit, and turns the others off.

  @Description
    Takes the index of one single digit to light up, and turn the rest ones off.

  @Param
   digit - index of the digit to light (0 to 3

  @Returns
   None
*/
void RNG_lightOneDigit(uint8_t digit);

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* RNG_H */

