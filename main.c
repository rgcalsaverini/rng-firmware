#include "mcc_generated_files/mcc.h"
#include "rng.h"

void main(void) {
    uint32_t pressed_count = 0;
    uint16_t number = 0;
    bool display = false;
    uint32_t timer_overflows = 0;
    uint8_t cycle = 0;
    uint8_t mode = 0;
    
    SYSTEM_Initialize();
    
    while (1) {    
        cycle++;
        
        if (PIN_BUTTON == LOW) {
            // If button is pressed for less than a long press, turn display off
            if (pressed_count < LONG_PRESS) {
                RNG_lightDigit(0, 0, 0, 0);
                pressed_count += 1;
            }
            // If this is a long press, change the mode to the next one
            else if (pressed_count == LONG_PRESS) {
                mode = (mode + 1) % 4;
                pressed_count += 1;
            }
            // If this is above the long press, stop incrementing and
            // just display the current mode
            else {
                RNG_display(mode, 10, 1);
                RNG_lightDigit(1, 0, 0, 0);

            }
        } else if(pressed_count > 0) {
            // If when the button was released, it was pressed for more than the
            // minimum amount of iterations, generate a new number and activate
            // the display flag.
            if (pressed_count > MIN_PRESS) {
                // We use pressed_count + cycle as salt. It might overflow, but
                // that is okay, we just want this to be a source of entropy
                number = RNG_generateRandomNumber(pressed_count + cycle,
                                                  mode_mod[mode]);
                display = true;
                // We also start the timer that will later turn off the display
                TMR2_StartTimer();
                timer_overflows = 0;
            }
            pressed_count = 0;
        }
        // If the button was not pressed and the display flag is on, we show
        // the number.
        else if(display) {
            RNG_display(number, mode_base[mode], mode_digits[mode]);
            
            if (TMR2_HasOverflowOccured()) {
                timer_overflows++;
            }
            
            // If the timer has overflowed more times than our sleep time, 
            // we turn off the display and stop the timer.
            if (timer_overflows > SLEEP_AFTER) {
                RNG_lightDigit(0, 0, 0, 0);
                display = false;
                timer_overflows = 0;
                TMR2_StopTimer();
            }
        }
    }
}
