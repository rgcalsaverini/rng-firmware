/*
 * File:   rng.c
 * Author: Rui Calsaverini
 *
 * Created on October 4, 2020, 6:44 PM
 */

#include "rng.h"
#include "mcc_generated_files/device_config.h"
#define LAST_VALUE_ADDR 0xA1C0

int timer_overflows = 0;
 
uint16_t RNG_generateRandomNumber(uint16_t salt, uint16_t mod) {
    srand(salt + rand());
    return (uint16_t) rand() % mod;
}

void RNG_display(uint16_t number, uint8_t num_base, uint8_t digits) {
    /*
     This will show 4 digits, one after the other. This way, the display is
     actually displaying one single digit at any specific time, but that happens
     so fast, that to the naked eye we see all four of them on.
     
     If you want to understand this better, set delay_between_digits to 250,
     and you will be able to see it.
     
     To show one specific digit, we must, in order:
     1. Turn off all digits
     2. Key the shift register to correctly represent one digit on the segments
     3. Turn on that specific digit.
     4. Apply a small delay
     
     If we do not do step 1, it will still work, but the shifting will be visible,
     and the dark segments will have a light glow that reduces the contrast of
     the display.
     
     The delay on the step 4 is to increase the time that the display stays
     lit, therefore increasing brightness. The longer the delay, the brighter 
     it will look, but also the more it will flicker. Empirically, for the
     clock of 20 MHz that we are using, 8ms seems to be a good middle value.
    */
    const unsigned long delay_between_digits = 8;
    
    for(uint8_t dig = 0 ; dig < 4 ; dig ++) { 
        if(digits > dig) {
            RNG_lightDigit(0, 0, 0, 0);
            RNG_displayOneDigit(number % num_base);
            RNG_lightOneDigit(dig);
            
            // Go to the next digit
            number /= num_base;
             __delay_ms(delay_between_digits);
        } else {
            /* If we skipped this else, the less digits we display, the brighter
             it would be, up to a point were a few seconds with digits = 1
             could burn the LEDs on the first digit. With this, we still apply
             the delay for the unused digits. Brightness will be uniform and
             the LEDs are safe :)
            */
            RNG_lightDigit(0, 0, 0, 0);
            __delay_ms(delay_between_digits);
        }
    }
}

void RNG_displayOneDigit(uint8_t number) {
    /*
    We first need to decode our number into the segments that we have on the
    display. For example, the number 1 lights up segments B and C, leaving
    the rest off, so it could be encoded into the binary 01100000, or the
    decimal 96. With this, we assembled a look up table called decode_matrix,
    that we then use to do it.
    
    Once we have the decoded number, we need to key (a.k.a. output) it into the
    shift register. That is done bit-by-bit (serially). We put the first bit 
    on the pin that is connected to the SR's data line, then pulse the clock
    once.
    After we do this for all the 8 bits of the number, the SR should be
    outputing the number we want.
    */
    
    uint8_t decoded_value = decode_matrix[number];
    
    for(uint8_t i = 0 ; i < 8 ; i++) {
        PIN_SR_CLOCK = 0;
        PIN_SR_DATA = decoded_value & 1;
        PIN_SR_CLOCK = 1;
        decoded_value  = decoded_value >> 1;
    }
    RC3 = 0;
}

void RNG_displayOneRaw(uint8_t number) {    
    for(uint8_t i = 0 ; i < 8 ; i++) {
        PIN_SR_CLOCK = 0;
        PIN_SR_DATA = number & 1;
        PIN_SR_CLOCK = 1;
        number  = number >> 1;
    }
    RC3 = 0;
}

#ifdef ANODE

void RNG_lightDigit(bool d1, bool d2, bool d3, bool d4) {
    PIN_D1 = PIN_D2 = PIN_D3 = PIN_D4 = 0;
    
    if(d1) PIN_D1 = 1;
    if(d2) PIN_D2 = 1;
    if(d3) PIN_D3 = 1;
    if(d4) PIN_D4 = 1;
}

void RNG_lightOneDigit(uint8_t digit) {
    PIN_D1 = PIN_D2 = PIN_D3 = PIN_D4 = 0;
    
    if(digit == 0) PIN_D1 = 1;
    else if(digit == 1) PIN_D2 = 1;
    else if(digit == 2) PIN_D3 = 1;
    else if(digit == 3) PIN_D4 = 1;
}

#else

void RNG_lightDigit(bool d1, bool d2, bool d3, bool d4) {
    PIN_D1 = PIN_D2 = PIN_D3 = PIN_D4 = 1;
    
    if(d1) PIN_D1 = 0;
    if(d2) PIN_D2 = 0;
    if(d3) PIN_D3 = 0;
    if(d4) PIN_D4 = 0;
}

void RNG_lightOneDigit(uint8_t digit) {
    PIN_D1 = PIN_D2 = PIN_D3 = PIN_D4 = 1;
    
    if(digit == 0) PIN_D1 = 0;
    else if(digit == 1) PIN_D2 = 0;
    else if(digit == 2) PIN_D3 = 0;
    else if(digit == 3) PIN_D4 = 0;
}

#endif